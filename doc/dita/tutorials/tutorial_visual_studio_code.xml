<?xml version='1.0' encoding='UTF-8'?>
<!-- This document was created with Syntext Serna Free. --><!DOCTYPE topic PUBLIC "-//OASIS//DTD DITA Topic//EN" "topic.dtd" []>
<topic id="topic-1">
  <title>Using Visual Studio Code with JAL </title>
  <prolog>
    <author>Rob Jansen</author>
    <publisher>Jallib Group</publisher>
  </prolog>
  <body>
    <section>
      <title>Introduction</title>
      <p>As explained in the <xref href="tutorial_getting_started.xml">
          <apiname>Getting Started </apiname>
        </xref>section, any text editor can be used to develop JAL programs. To make life easier  JAL Edit can be used, which supports syntax highlighting and much more. This editor, however, only works for Windows. </p>
      <p>A good editor helps writing good programs and syntax highlighting makes life easier. In order to support more than only the Windows platform, Microsoft Visual Studio Code can be used, which runs on Windows, Linux and MacOS. Visual Studio Code can be <xref href="https://code.visualstudio.com/" format="html" scope="external">
          <apiname>downloaded</apiname>
        </xref> for free. A JAL Visual Studio Code extension is available  to support programming in JAL. </p>
      <p>This section describes how to install the Visual Studio Code JAL extension and how to use Visual Studio Code for your JAL development. </p>
    </section>
    <section>
      <title>Downloading the JAL Visual Studio Code extension</title>
      <p>After having installed Visual Studio Code you can download the VS Code JAL extension from the VS Code extension market place.

The steps are as follows. </p>
      <p>From View --&gt; Extensions open the extensions bar. which will appear bar on the left side of the screen.</p>
      <p><image href="images/visual_studio_code_extension.png" otherprops="clickable"/></p>
      <p>
    Type ‘jal’ in the search bar and you will find the JAL extension.</p>
      <p><image href="images/visual_studio_code_jal.png" otherprops="clickable"/></p>
      <p>Now install the extension.</p>
    </section>
    <section>
      <title>Installing a task to run the compiler</title>
      <p>Select Terminal --&gt; Configure Tasks ...</p>
      <p><image href="images/visual_studio_code_configure.png" otherprops="clickable"/></p>
      <p>In the <codeph>tasks.json</codeph> replace the example by the following:</p>
      <p><image href="images/visual_studio_code_tasks.png" width="400" otherprops="clickable"/></p>
      <p>You can download this piece of code from <xref href="https://github.com/jallib/jallib/blob/master/tools/VS%20Code%20tasks.json" scope="external" format="html">
          <apiname>GitHub</apiname>
        </xref> or copy-paste the code below in the <codeph>tasks.json</codeph>.</p>
      <p><codeblock scale="80">{
    // See https://go.microsoft.com/fwlink/?LinkId=733558
    // for the documentation about the tasks.json format
    // prefilled tasks.json for compiling a JAL file
    &quot;version&quot;: &quot;2.0.0&quot;,
    &quot;tasks&quot;: [
        {
            &quot;label&quot;: &quot;Compile JAL File&quot;,
            &quot;type&quot;: &quot;process&quot;,
            &quot;command&quot;: &quot;${config:jal.paths.exePath}&quot;,
            &quot;args&quot;: [
                &quot;${file}&quot;,
                &quot;-s&quot;,
                &quot;${config:jal.paths.LibPath}&quot;
            ],
            &quot;presentation&quot;: {
                &quot;reveal&quot;: &quot;always&quot;,
                &quot;panel&quot;: &quot;new&quot;
            },
            &quot;problemMatcher&quot;: [],
            &quot;group&quot;: {
                &quot;kind&quot;: &quot;build&quot;,
                &quot;isDefault&quot;: true
            }
        }
    ]
}</codeblock></p>
    </section>
    <section>
      <title>Configuring the JAL extension</title>
      <p>On the installed plug-in click on the ‘manage’ (or the configuration) wheel.</p>
      <p><image href="images/visual_studio_code_manage.png" otherprops="clickable"/></p>
      <p>Select Extension Settings.</p>
      <image href="images/visual_studio_code_settings.png" otherprops="clickable"/>
      <p>Set the paths to the JAL documentation, the JAL compiler, the JAL libraries and programmer (if present), an example is given below:</p>
      <p><image href="images/visual_studio_code_paths.png" width="400" otherprops="clickable"/></p>
    </section>
    <section>
      <title>The final result</title>
      <p>When done, your JAL blink-a-led program could look like this:</p>
      <p><image href="images/visual_studio_code_blink_a_led.png" width="400" otherprops="clickable"/></p>
      <p>You can select your own theme  to suit your needs.</p>
    </section>
    <section>
      <title>Activating the compiler</title>
      <p>You can set the default build task to run the compiler under Terminal --&gt; Configure Default Build Task ...</p>
      <p><image href="images/visual_studio_code_build.png" otherprops="clickable"/></p>
      <p>The compiler can be activated by:</p>
      <ul>
        <li>Terminal --&gt; Run Task … and selecting the task Compile JAL File</li>
        <li>Terminal --&gt; Run Buld Task ...</li>
        <li>Using the shortcut <codeph>ctrl-shift-b</codeph></li>
      </ul>
      <p><note>If you have more files open in your editor, always activate the compiler form the main program.</note> </p>
      <p>The compiler output appears at the bottom of the screen in a terminal window and may look like this: </p>
      <p><image href="images/visual_studio_code_output.png" otherprops="clickable"/> </p>
      <p>If a compiler error occurs you can jump to that error in the editor by pressing <codeph>ctrl-click</codeph>. </p>
      <p><image href="images/visual_studio_code_error.png" width="400" otherprops="clickable"/> </p>
    </section>
    <section>
      <title>JAL VS Code extension features</title>
      <p> Features that work well:</p>
      <ul>
        <li>Syntax Highlighting</li>
        <li>Fast opening and saving of files irrespective of the size </li>
        <li>Linux and Windows support Code folding</li>
        <li>Auto completion (More work needs to be done to have functions/procedures included from include files)</li>
        <li>Code Snippets (Only very few are added, but not very difficult to add)</li>
        <li>Compiling to Hex file</li>
        <li>Ctrl-Click to go to error line (It doesn&apos;t go automatically)</li>
        <li>Searching/Replacing any word within files and across folder</li>
        <li>Direct Github Push/Pull/Diff other commands</li>
        <li>Side by side View and file comparison</li>
        <li>Theme selection/switching</li>
        <li>Folder/explorer view </li>
      </ul>
      <p>Some of the main features not in the extension: </p>
      <ul>
        <li>Code Explorer for include files,procedures,functions,variables,constants,aliases</li>
        <li>Opening include files with Ctrl-Enter</li>
        <li>Auto Backup with time stamp/compilation</li>
        <li>Backup project as zip file</li>
        <li>Go to error line after compilation</li>
        <li>Running programmer on successful build</li>
        <li>Serial Terminal</li>
        <li>Compile/Program buttons/keys</li>
        <li>Detecting PIC name from code and passing it as variable to Programmer executable</li>
        <li>Set file as Active JAL file and compile that irrespective of file you are editing </li>
      </ul>
    </section>
  </body>
</topic>
